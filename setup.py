#!/usr/bin/env python

from distutils.core import setup
from distutils.command.sdist import sdist
from distutils.command.install_data import install_data
from distutils.command.build_scripts import build_scripts
from glob import glob
import os

########################################################################
# Had to overwrite the prunefile_list method of sdist to not
# remove automatically the RCS/CVS directory from the distribution.
########################################################################

class modified_sdist(sdist):
    def prune_file_list(self):
        """
        Prune off branches that might slip into the file list as created
        by 'read_template()', but really don't belong there:
          * the build tree (typically 'build')
          * the release tree itself (only an issue if we ran 'sdist
            previously with --keep-temp, or it aborted)
        """
        build = self.get_finalized_command('build')
        base_dir = self.distribution.get_fullname()
        self.filelist.exclude_pattern(None, prefix=build.build_base)
        self.filelist.exclude_pattern(None, prefix=base_dir)

class modified_install_data(install_data):

    def run(self):
        install_cmd = self.get_finalized_command('install')
        self.install_dir = getattr(install_cmd, 'install_lib')
        return install_data.run(self)
    
class modified_build_scripts(build_scripts):
    
    def copy_scripts(self):
        #print "scripts:" , self.scripts

        for script in self.scripts:
            fnew = open(script, "w")
            forig = open(script+".py", "r")
            txt = forig.readlines()
            forig.close()
            fnew.write("#!/usr/bin/env python2.5\n")
            fnew.writelines(txt)
            fnew.close()
        build_scripts.copy_scripts(self)

        
########################################################################
# list of the python packages to be included in this distribution.
# sdist doesn't go recursively into subpackages so they need to be
# explicitaly listed.
# From these packages only the python modules will be taken
packages = ['CADD',
            'CADD/workflows/docking',
            'CADD/workflows/MDanalysis' , #scripts
            'CADD/workflows/user',
            'CADD/workflows/virtualScreening',
            'CADD/workflows/visualization',
            'CADD/Raccoon',
            'CADD/Raccoon/icons',
            'CADD/Raccoon2',
            'CADD/Raccoon2/gui',
            ]

# list of the python modules which are not part of a package
py_modules = ['CADD/bin/runCADD',
              ]

# list of the files that are not python packages but are included in the
# distribution and need to be installed at the proper place  by distutils.
# The list in MANIFEST.in lists is needed for including those files in
# the distribution, data_files in setup.py is needed to install them
# at the right place.
data_files = []

def getDataFiles(file_list, directory, names):
    fs = []
    for name in names:
        ext = os.path.splitext(name)[1]
        #print directory, name, ext, len(ext)
        if ext !=".py" and ext !=".pyc":
            fullname = os.path.join(directory,name)
            if not os.path.isdir(fullname):
                fs.append(fullname)
    if len(fs):
        file_list.append((directory, fs))

os.path.walk("CADD", getDataFiles, data_files)

scripts = ['CADD/bin/runCADD']
# description of what is going to be included in the distribution and
# installed.
from version import VERSION
setup(name = 'CADD',
      version = VERSION,
      description = "CADD - A computer aided drug design using Vision",
      author = 'UCSD',
      author_email = 'wili@ucsd.edu',
      download_url = 'http://nbcr.sdsc.edu/pub/wiki/index.php?title=CADD_Pipeline',
      url = 'http://nbcr.sdsc.edu/pub/wiki/index.php?title=CADD_Pipeline',
      packages = packages,
      py_modules = py_modules,
      data_files = data_files,
      scripts = scripts,
      cmdclass = {'sdist': modified_sdist,
                  'install_data': modified_install_data,
                  'build_scripts': modified_build_scripts
                  },
      )
